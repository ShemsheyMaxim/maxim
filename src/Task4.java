/**
 * Created by Максим on 28.02.2017.
 */
public class Task4 {
    public static void main(String[] args) {
        int bank = 5000;
        String uah = "UAH =";
        double gbp = 33.58551;
        double eur = 28.641723;
        double usd = 27.053672;
        double inUsd = bank / usd;
        double inEur = bank / eur;
        double inGbp = bank / gbp;
        System.out.println(bank + " " + uah + " " + inUsd + " " + "USD");
        System.out.println(bank + " " + uah + " " + inEur + " " + "EUR");
        System.out.println(bank + " " + uah + " " + inGbp + " " + "GBP");
    }
}
