/**
 * Created by Максим on 28.02.2017.
 */
public class Task3 {
    public static void main(String[] args) {
        double ukraineKillometrs = 603628;
        double spainKillometrs = 504782;
        double englandKillometrs = 133396;

        double meter = 1000000;
        double mile = 0.386;

        double ukraineMeter = ukraineKillometrs * meter;
        System.out.println("Площадь Украины:" + " " + ukraineMeter + " " + "кв метров");

        double spainMeter = spainKillometrs * meter;
        System.out.println("Площадь Испании:" + " " + spainMeter + " " + "кв метров");

        double englandMeter = englandKillometrs * meter;
        System.out.println("Площадь Англии:" + " " + englandMeter + " " + "кв метров");

        double ukraineMile = ukraineKillometrs * mile;
        System.out.println("Площадь Украины:" + " " + ukraineMile + " " + "кв миль");

        double spaineMile = spainKillometrs * mile;
        System.out.println("Площадь Испании:" + " " + spaineMile + " " + "кв миль");

        double englandMile = englandKillometrs * mile;
        System.out.println("Площадь Англии:" + " " + englandMile + " " + "кв миль");
    }
}
