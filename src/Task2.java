/**
 * Created by Максим on 28.02.2017.
 */
public class Task2 {
    public static void main(String[] args) {
        String kmToTheSun = "км до Солнца";
        int fromMercuryToSun = 57910000;
        int fromVenusToSun = 108200000;
        int fromEarthToSun = 149600000;
        int fromMarsToSun = 227900000;
        System.out.println("Меркурий:" + " " + fromMercuryToSun + " " + kmToTheSun);
        System.out.println("Венера:" + " " + fromVenusToSun + " " + kmToTheSun);
        System.out.println("Земля:" + " " + fromEarthToSun + " " + kmToTheSun);
        System.out.println("Марс:" + " " + fromMarsToSun + " " + kmToTheSun);
        int fromMercuryToEarth = fromEarthToSun - fromMercuryToSun;
        int fromVenusToEarth = fromEarthToSun - fromVenusToSun;
        int fromMarsToEarth = fromMarsToSun - fromEarthToSun;
        System.out.println("От Меркурия до Земли" + " " + fromMercuryToEarth + " " + "км");
        System.out.println("От Венеры до Земли" + " " + fromVenusToEarth + " " + "км");
        System.out.println("От Марса до Земли" + " " + fromMarsToEarth + " " + "км");
    }
}
