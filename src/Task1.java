/**
 * Created by Максим on 28.02.2017.
 */
public class Task1 {
    public static void main(String[] args) {
        int ukraineSquare = 45490000;
        int englandSquare = 53010000;
        int franceSquare = 64200000;
        int spainSquare = 46438422;
        int italiaSquare = 62000000;
        System.out.println("Население Украины:" + " " + ukraineSquare + " " + "человек");
        System.out.println("Население Англии:" + " " + englandSquare + " " + "человек");
        System.out.println("Население Франции:" + " " + franceSquare + " " + "человек");
        System.out.println("Население Испании:" + " " + spainSquare + " " + "человек");
        System.out.println("Население Италии:" + " " + italiaSquare + " " + "человек");
    }
}
